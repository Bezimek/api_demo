import requests

"""
200 – OK. The request was successful. The answer itself depends on the method used (GET, POST, etc.) and the API specification.
204 – No Content. The server successfully processed the request and did not return any content.
301 – Moved Permanently. The server responds that the requested page (endpoint) has been moved to another address and redirects to this address.
400 – Bad Request. The server cannot process the request because the client-side errors (incorrect request format).
401 – Unauthorized. Occurs when authentication was failed, due to incorrect credentials or even their absence.
403 – Forbidden. Access to the specified resource is denied.
404 – Not Found. The requested resource was not found on the server.
500 – Internal Server Error. Occurs when an unknown error has occurred on the server.
"""
url_tables = 'http://api.nbp.pl/api/exchangerates/tables/'
url_rates = 'http://api.nbp.pl/api/exchangerates/rates/'

tables = ['A/', 'B/', 'C/']
date = 'today'
codes = ['PLN/', 'EUR/', "RUB/"]
parameters = {'format' : 'json'}
response_tables = requests.get(url_tables + tables[0], params= parameters)
response_rates = requests.request('GET', url_rates + tables[2] + codes[1], params = parameters)
print(response_tables)
print(response_rates.content)
