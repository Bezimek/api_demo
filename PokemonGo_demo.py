import requests

class PokemonGo:

    def __init__(self, option = 0):
        self.option = option
        self.__url = 'https://pogoapi.net/'
        self.__routes = {
            'types' : '/api/v1/weather_boosts.json',
            'pokemon_types' : '/api/v1/pokemon_types.json'
        }

    def get_types(self):
        response = requests.request('GET', self.__url + self.__routes['types'])
        pokemon_types = [response.json()[key] for key in response.json()]
        pokemon_types = [pokemon_type for sublist in pokemon_types for pokemon_type in sublist]
        return pokemon_types

    def count_types(self):
        summary = {}
        database = self.get_pokemons_types()

        for pokemon_type in self.get_types():
            summary[pokemon_type] = 0

        for pokemon in database:
            for element in database[pokemon]:
                summary[element] += 1
        return summary

    def get_pokemons_types(self):
        response = requests.request('GET', self.__url + self.__routes['pokemon_types'])
        database = {}
        for pokemon in response.json():
            if pokemon['form'] == 'Normal':
                database[pokemon['pokemon_name']] = pokemon['type']
        return database

test = PokemonGo()
#print(test.get_types())
#print(test.get_pokemons()['Bulbasaur'])
print(test.count_types())
